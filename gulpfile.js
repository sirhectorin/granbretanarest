var gulp = require("gulp");
var ts = require("gulp-typescript");
var nodemon = require("nodemon");
var gnodemon = require("gulp-nodemon");
var tsProject = ts.createProject("tsconfig.json");

function tsCompile() {
    return tsProject.src()
        .pipe(tsProject())
        .js.pipe(gulp.dest("build"));
}

function StartNodemon() {
    gnodemon({
        script: 'build\\index.js'
    });
    return;
}
gulp.task('default', gulp.series(tsCompile, StartNodemon));
gulp.watch('**/*.ts', tsCompile);