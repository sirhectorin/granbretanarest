import express from 'express';
import cors from 'cors';
import morgan from 'morgan';
import { createConnection } from 'typeorm';
import { productoController } from './controllers/ProductoController';
import { usuarioController } from './controllers/UsuarioController';

class Server {
    app : express.Application;
    constructor(){
        this.app = express();
        this.app.use(morgan('dev'));
        this.app.use(cors());
        this.app.use(express.json());
        this.app.use(express.urlencoded({extended:false}));
        this.rutas();
        this.start();
    }

    private rutas(){
        this.app.use( "/producto", productoController.getRouter());
        // this.app.use( "/usuario", usuarioController.getRouter());
    }

    start():void{
        createConnection().then(conexion => {
            this.app.listen("3000", ()=>{
                console.log("Escuchando en el puerto 3000");
            });
        });
    }
}

new Server();