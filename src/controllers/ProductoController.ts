import { Request, Response } from 'express';
import { Router } from "express"
import producto from '../model/producto';

class ProductoController{

    public getRouter(): Router{
        let router : Router = Router();
        router.get("/", this.listar);
        router.get("/:id", this.buscar);
        router.post("/", this.agregar);
        router.delete("/:id", this.eliminar);
        router.put("/:id", this.actualizar);
        return router;  
    }

    async listar(req: Request, res : Response){
        producto.find().then(prods => res.json(prods), error => res.status(500).send(error));      
    }

    async buscar(req: Request, res : Response){
        const { id } = req.params;
        producto.find({
            where: {cod_producto: id}, 
            relations: [ "proveedorProductos.proveedor", "proveedorProductos"]
        }).then(prods => res.json(prods), error => res.status(500).send(error))
    }

    async agregar(req:Request, res : Response){ // not implemented yet
        let producto : producto = req.body;
        res.json(producto);
    }

    async eliminar(req: Request, res : Response){
        const { id } = req.params;
        producto.find({cod_producto: id}).then(async (prod : producto[]) =>{
            if(prod.length != 0){
                let retorno = await prod[0].remove();
                typeof retorno == typeof producto ? res.send(retorno) : res.status(500).send(retorno)
            }else{
                return res.status(404).send("Producto no encontrado")
            }
            
        }, (error:any) => res.status(404).send(error));
    }

    async actualizar(req: Request, res : Response){
        const { id } = req.params;
        
        producto.findOne({cod_producto: id}).then(async (producto : producto) =>{
            const prodModificado : producto = req.body;
            prodModificado.cod_producto = id;
            let retorno = prodModificado.save();
            //const producto : Producto = productos[0];
            typeof retorno == typeof prodModificado ? res.send(retorno) : res.status(500).send(retorno)
        }, error => res.status(404).send(error));
    }
}

export const productoController = new ProductoController();