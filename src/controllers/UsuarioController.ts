import { Request, Response } from 'express';
import { Router } from "express";
import usuario from '../model/usuario';
import { reject } from 'bluebird';

class UsuarioController{

    /*public getRouter() : Router{
        let router : Router = Router();
        router.get("/", usuarioController.listar.bind(usuarioController));
        router.get("/:rut", usuarioController.buscar.bind(usuarioController));
        router.post("/", usuarioController.agregar.bind(usuarioController));
        router.delete("/:rut", usuarioController.eliminar.bind(usuarioController));
        router.put("/:rut", usuarioController.actualizar.bind(usuarioController));
        return router;
    }

    async listar(req: Request, res : Response){
        this.todos().then(
            (listado : Array<usuario>) => res.json(listado),
            error => res.status(500).send(error)
        );        
    }

    async buscar(req: Request, res : Response){
        const { rut } = req.params;
        this.encontrar(rut).then((usuario : usuario) =>{
            res.json(usuario);
        }, error => res.status(404).send(error));
    }

    async agregar(req:Request, res : Response){ // not implemented yet
        let usuario : usuario = req.body;
        res.json(usuario);
    }

    async eliminar(req: Request, res : Response){
        const { rut } = req.params;
        this.encontrar(rut).then(async (usuario : usuario) =>{
            const retorno = await pool.query("DELETE FROM Usuario where rut = ?", [rut]);
            res.send("Usuario " + (retorno == 1 ? "eliminado" : "no eliminado"));
        }, error => res.status(404).send(error));
    }

    async actualizar(req: Request, res : Response){
        const { rut } = req.params;
        
        this.encontrar(rut).then(async (usuario : usuario) =>{
            const prodModificado : usuario = req.body;
            //const usuario : usuario = usuarios[0];
    
            const retorno = await pool.query("UPDATE Usuario set ? where rut = ?", [prodModificado, rut])
            res.send("Usuario actualizado exitosamente");
        }, error => res.status(404).send(error));
    }

    // NOT public

    private async encontrar(rut : string) : Promise<usuario> {
        try{
            const usuarios : Array<usuario> = await pool.query(`SELECT * from Usuario where rut = ?`, [rut]);
            if(usuarios.length > 0)
                return usuarios[0];
            return reject('usuario no encontrado');
        } catch (err){
            return reject(err);
        }
    }

    private async todos() : Promise<Array<usuario>>{
        try{
            return await pool.query(`SELECT * from Usuario`);
        } catch(err) {
            return reject(err);
        } 
    }*/
}

export const usuarioController = new UsuarioController();