import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {venta} from "./venta";
import producto from "./producto";


@Entity("detalle_venta",{schema:"rotiseria" } )
@Index("fk_producto_detalleventa",["codProducto",])
export class detalle_venta {

   
    @ManyToOne(type=>venta, venta=>venta.detalleVentas,{ primary:true, nullable:false,onDelete: 'RESTRICT',onUpdate: 'RESTRICT' })
    @JoinColumn({ name:'cod_venta'})
    codVenta:venta | null;


   
    @ManyToOne(type=>producto, producto=>producto.detalleVentas,{ primary:true, nullable:false,onDelete: 'RESTRICT',onUpdate: 'RESTRICT' })
    @JoinColumn({ name:'cod_producto'})
    codProducto:producto | null;


    @Column("int",{ 
        nullable:true,
        name:"cantidad"
        })
    cantidad:number | null;
        

    @Column("int",{ 
        nullable:true,
        name:"precio"
        })
    precio:number | null;
        

    @Column("int",{ 
        nullable:true,
        name:"subtotal"
        })
    subtotal:number | null;
        
}
