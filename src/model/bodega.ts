import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {ajuste_inventario} from "./ajuste_inventario";
import {compra} from "./compra";
import {stock} from "./stock";


@Entity("bodega",{schema:"rotiseria" } )
export class bodega {

    @Column("tinyint",{ 
        nullable:false,
        primary:true,
        name:"cod_bodega"
        })
    cod_bodega:number;
        

    @Column("varchar",{ 
        nullable:false,
        length:20,
        name:"nombre"
        })
    nombre:string;
        

    @Column("varchar",{ 
        nullable:false,
        length:40,
        name:"direccion"
        })
    direccion:string;
        

   
    @OneToMany(type=>ajuste_inventario, ajuste_inventario=>ajuste_inventario.codBodega,{ onDelete: 'RESTRICT' ,onUpdate: 'RESTRICT' })
    ajusteInventarios:ajuste_inventario[];
    

   
    @OneToMany(type=>compra, compra=>compra.codBodega,{ onDelete: 'RESTRICT' ,onUpdate: 'RESTRICT' })
    compras:compra[];
    

   
    @OneToMany(type=>stock, stock=>stock.codBodega,{ onDelete: 'RESTRICT' ,onUpdate: 'RESTRICT' })
    stocks:stock[];
    
}
