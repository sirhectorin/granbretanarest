import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {compra} from "./compra";
import producto from "./producto";


@Entity("detalle_compra",{schema:"rotiseria" } )
@Index("fk_producto_detallecompra",["codProducto",])
export class detalle_compra {

   
    @ManyToOne(type=>compra, compra=>compra.detalleCompras,{ primary:true, nullable:false,onDelete: 'RESTRICT',onUpdate: 'RESTRICT' })
    @JoinColumn({ name:'cod_compra'})
    codCompra:compra | null;


   
    @ManyToOne(type=>producto, producto=>producto.detalleCompras,{ primary:true, nullable:false,onDelete: 'RESTRICT',onUpdate: 'RESTRICT' })
    @JoinColumn({ name:'cod_producto'})
    codProducto:producto | null;


    @Column("int",{ 
        nullable:true,
        name:"cantidad"
        })
    cantidad:number | null;
        

    @Column("int",{ 
        nullable:true,
        name:"precio"
        })
    precio:number | null;
        

    @Column("int",{ 
        nullable:true,
        name:"subtotal"
        })
    subtotal:number | null;
        
}
