import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import producto from "./producto";
import {bodega} from "./bodega";
import usuario from "./usuario";


@Entity("ajuste_inventario",{schema:"rotiseria" } )
@Index("fk_producto_ajuste",["codProducto",])
@Index("fk_usuario_ajuste",["rut",])
@Index("fk_bodega_ajuste",["codBodega",])
export class ajuste_inventario {

    @Column("date",{ 
        nullable:false,
        primary:true,
        name:"fecha"
        })
    fecha:string;
        

    @Column("time",{ 
        nullable:false,
        primary:true,
        name:"hora"
        })
    hora:string;
        

    @Column("varchar",{ 
        nullable:false,
        primary:true,
        length:20,
        name:"tipo_ajuste"
        })
    tipo_ajuste:string;
        

   
    @ManyToOne(type=>producto, producto=>producto.ajusteInventarios,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'cod_producto'})
    codProducto:producto | null;


   
    @ManyToOne(type=>bodega, bodega=>bodega.ajusteInventarios,{ onDelete: 'RESTRICT',onUpdate: 'RESTRICT' })
    @JoinColumn({ name:'cod_bodega'})
    codBodega:bodega | null;


    @Column("tinyint",{ 
        nullable:true,
        name:"cantidad"
        })
    cantidad:number | null;
        

    @Column("text",{ 
        nullable:true,
        name:"descripcion"
        })
    descripcion:string | null;
        

   
    @ManyToOne(type=>usuario, usuario=>usuario.ajusteInventarios,{ onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'rut'})
    rut:usuario | null;

}
