import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {compra} from "./compra";
import {proveedor_producto} from "./proveedor_producto";


@Entity("proveedor",{schema:"rotiseria" } )
export class proveedor {

    @Column("varchar",{ 
        nullable:false,
        primary:true,
        length:10,
        name:"rut"
        })
    rut:string;
        

    @Column("varchar",{ 
        nullable:false,
        length:50,
        name:"nombre"
        })
    nombre:string;
        

    @Column("varchar",{ 
        nullable:false,
        length:10,
        name:"telefono"
        })
    telefono:string;
        

    @Column("varchar",{ 
        nullable:false,
        length:60,
        name:"correo"
        })
    correo:string;
        

   
    @OneToMany(type=>compra, compra=>compra.rutProveedor,{ onDelete: 'RESTRICT' ,onUpdate: 'RESTRICT' })
    compras:compra[];
    

   
    @OneToMany(type=>proveedor_producto, proveedor_producto=>proveedor_producto.proveedor,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    proveedorProductos:proveedor_producto[];
    
}
