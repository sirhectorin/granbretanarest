import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import producto from "./producto";
import {proveedor} from "./proveedor";


@Entity("proveedor_producto",{schema:"rotiseria" } )
@Index("rut_proveedor",["producto", "proveedor"])
export class proveedor_producto {

   
    @ManyToOne(type=>producto, producto=>producto.proveedorProductos,{ primary:true, nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'cod_producto'})
    producto:producto | null;


   
    @ManyToOne(type=>proveedor, proveedor=>proveedor.proveedorProductos,{ primary:true, nullable:false,onDelete: 'RESTRICT',onUpdate: 'CASCADE' })
    @JoinColumn({ name:'rut_proveedor'})
    proveedor:proveedor | null;


    @Column("int",{ 
        nullable:true,
        name:"precio"
        })
    precio:number | null;
        

    @Column("int",{ 
        nullable:true,
        name:"cantidad"
        })
    cantidad:number | null;
        
}
