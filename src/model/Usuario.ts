import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {ajuste_inventario} from "./ajuste_inventario";
import {venta} from "./venta";


@Entity("usuario",{schema:"rotiseria" } )
export default class usuario {

    @Column("varchar",{ 
        nullable:false,
        primary:true,
        length:10,
        name:"rut"
        })
    rut:string;
        

    @Column("varchar",{ 
        nullable:false,
        length:40,
        name:"nombre"
        })
    nombre:string;
        

    @Column("varchar",{ 
        nullable:true,
        length:8,
        name:"telefono"
        })
    telefono:string | null;
        

    @Column("tinyint",{ 
        nullable:false,
        name:"acceso"
        })
    acceso:number;
        

    @Column("varchar",{ 
        nullable:false,
        length:256,
        name:"clave"
        })
    clave:string;
        

   
    @OneToMany(type=>ajuste_inventario, ajuste_inventario=>ajuste_inventario.rut,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    ajusteInventarios:ajuste_inventario[];
    

   
    @OneToMany(type=>venta, venta=>venta.rutUsuario,{ onDelete: 'RESTRICT' ,onUpdate: 'RESTRICT' })
    ventas:venta[];
    
}
