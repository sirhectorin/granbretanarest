import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {metodo_pago} from "./metodo_pago";
import usuario from "./usuario";
import {detalle_venta} from "./detalle_venta";


@Entity("venta",{schema:"rotiseria" } )
@Index("fk_metodo_venta",["metodoPago",])
@Index("fk_usuario_venta",["rutUsuario",])
export class venta {

    @PrimaryGeneratedColumn({
        type:"bigint", 
        name:"cod_venta"
        })
    cod_venta:string;
        

   
    @ManyToOne(type=>metodo_pago, metodo_pago=>metodo_pago.ventas,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'RESTRICT' })
    @JoinColumn({ name:'metodo_pago'})
    metodoPago:metodo_pago | null;


   
    @ManyToOne(type=>usuario, usuario=>usuario.ventas,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'RESTRICT' })
    @JoinColumn({ name:'rut_usuario'})
    rutUsuario:usuario | null;


    @Column("date",{ 
        nullable:false,
        name:"fecha"
        })
    fecha:string;
        

    @Column("int",{ 
        nullable:true,
        name:"total_venta"
        })
    total_venta:number | null;
        

    @Column("bigint",{ 
        nullable:true,
        name:"nro_boleta"
        })
    nro_boleta:string | null;
        

   
    @OneToMany(type=>detalle_venta, detalle_venta=>detalle_venta.codVenta,{ onDelete: 'RESTRICT' ,onUpdate: 'RESTRICT' })
    detalleVentas:detalle_venta[];
    
}
