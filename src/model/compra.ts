import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {bodega} from "./bodega";
import {proveedor} from "./proveedor";
import {metodo_pago} from "./metodo_pago";
import {detalle_compra} from "./detalle_compra";


@Entity("compra",{schema:"rotiseria" } )
@Index("fk_metodo_compra",["metodoPago",])
@Index("fk_proveedor_compra",["rutProveedor",])
@Index("fk_bodega_compra",["codBodega",])
export class compra {

    @PrimaryGeneratedColumn({
        type:"bigint", 
        name:"cod_compra"
        })
    cod_compra:string;
        

   
    @ManyToOne(type=>bodega, bodega=>bodega.compras,{ onDelete: 'RESTRICT',onUpdate: 'RESTRICT' })
    @JoinColumn({ name:'cod_bodega'})
    codBodega:bodega | null;


   
    @ManyToOne(type=>proveedor, proveedor=>proveedor.compras,{ onDelete: 'RESTRICT',onUpdate: 'RESTRICT' })
    @JoinColumn({ name:'rut_proveedor'})
    rutProveedor:proveedor | null;


    @Column("date",{ 
        nullable:true,
        name:"fecha"
        })
    fecha:string | null;
        

    @Column("int",{ 
        nullable:true,
        name:"total_compra"
        })
    total_compra:number | null;
        

   
    @ManyToOne(type=>metodo_pago, metodo_pago=>metodo_pago.compras,{ onDelete: 'RESTRICT',onUpdate: 'RESTRICT' })
    @JoinColumn({ name:'metodo_pago'})
    metodoPago:metodo_pago | null;


    @Column("bigint",{ 
        nullable:true,
        name:"nro_factura"
        })
    nro_factura:string | null;
        

   
    @OneToMany(type=>detalle_compra, detalle_compra=>detalle_compra.codCompra,{ onDelete: 'RESTRICT' ,onUpdate: 'RESTRICT' })
    detalleCompras:detalle_compra[];
    
}
