import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import producto from "./producto";
import {bodega} from "./bodega";


@Entity("stock",{schema:"rotiseria" } )
@Index("fk_bodega_producto",["codBodega",])
export class stock {

   
    @ManyToOne(type=>producto, producto=>producto.stocks,{ primary:true, nullable:false,onDelete: 'RESTRICT',onUpdate: 'RESTRICT' })
    @JoinColumn({ name:'cod_producto'})
    codProducto:producto | null;


   
    @ManyToOne(type=>bodega, bodega=>bodega.stocks,{ primary:true, nullable:false,onDelete: 'RESTRICT',onUpdate: 'RESTRICT' })
    @JoinColumn({ name:'cod_bodega'})
    codBodega:bodega | null;


    @Column("int",{ 
        nullable:false,
        name:"stock"
        })
    stock:number;
        
}
