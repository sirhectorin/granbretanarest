import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {compra} from "./compra";
import {venta} from "./venta";


@Entity("metodo_pago",{schema:"rotiseria" } )
export class metodo_pago {

    @Column("varchar",{ 
        nullable:false,
        primary:true,
        length:30,
        name:"nombre"
        })
    nombre:string;
        

   
    @OneToMany(type=>compra, compra=>compra.metodoPago,{ onDelete: 'RESTRICT' ,onUpdate: 'RESTRICT' })
    compras:compra[];
    

   
    @OneToMany(type=>venta, venta=>venta.metodoPago,{ onDelete: 'RESTRICT' ,onUpdate: 'RESTRICT' })
    ventas:venta[];
    
}
