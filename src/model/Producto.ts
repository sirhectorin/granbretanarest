import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {ajuste_inventario} from "./ajuste_inventario";
import {detalle_compra} from "./detalle_compra";
import {detalle_venta} from "./detalle_venta";
import {proveedor_producto} from "./proveedor_producto";
import {stock} from "./stock";


@Entity("producto",{schema:"rotiseria" } )
export default class producto extends BaseEntity{

    @Column("bigint",{ 
        nullable:false,
        primary:true,
        name:"cod_producto"
        })
    cod_producto:string;
        

    @Column("varchar",{ 
        nullable:false,
        length:50,
        name:"nombre"
        })
    nombre:string;
        

    @Column("int",{ 
        nullable:false,
        name:"precio"
        })
    precio:number;
        

    @Column("varchar",{ 
        nullable:false,
        length:7,
        default: () => "'unidad'",
        name:"unidad_medida"
        })
    unidad_medida:string;
        

    @Column("tinyint",{ 
        nullable:false,
        default: () => "'5'",
        name:"stock_critico"
        })
    stock_critico:number;
        

   
    @OneToMany(type=>ajuste_inventario, ajuste_inventario=>ajuste_inventario.codProducto,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    ajusteInventarios:ajuste_inventario[];
    

   
    @OneToMany(type=>detalle_compra, detalle_compra=>detalle_compra.codProducto,{ onDelete: 'RESTRICT' ,onUpdate: 'RESTRICT' })
    detalleCompras:detalle_compra[];
    

   
    @OneToMany(type=>detalle_venta, detalle_venta=>detalle_venta.codProducto,{ onDelete: 'RESTRICT' ,onUpdate: 'RESTRICT' })
    detalleVentas:detalle_venta[];
    

   
    @OneToMany(type=>proveedor_producto, proveedor_producto=>proveedor_producto.producto,{ onDelete: 'RESTRICT' ,onUpdate: 'CASCADE' })
    proveedorProductos:proveedor_producto[];
    

   
    @OneToMany(type=>stock, stock=>stock.codProducto,{ onDelete: 'RESTRICT' ,onUpdate: 'RESTRICT' })
    stocks:stock[];
    
}
