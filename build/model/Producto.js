"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
const typeorm_1 = require("typeorm");
const ajuste_inventario_1 = require("./ajuste_inventario");
const detalle_compra_1 = require("./detalle_compra");
const detalle_venta_1 = require("./detalle_venta");
const proveedor_producto_1 = require("./proveedor_producto");
const stock_1 = require("./stock");
let producto = class producto extends typeorm_1.BaseEntity {
};
__decorate([
    typeorm_1.Column("bigint", {
        nullable: false,
        primary: true,
        name: "cod_producto"
    }),
    __metadata("design:type", String)
], producto.prototype, "cod_producto", void 0);
__decorate([
    typeorm_1.Column("varchar", {
        nullable: false,
        length: 50,
        name: "nombre"
    }),
    __metadata("design:type", String)
], producto.prototype, "nombre", void 0);
__decorate([
    typeorm_1.Column("int", {
        nullable: false,
        name: "precio"
    }),
    __metadata("design:type", Number)
], producto.prototype, "precio", void 0);
__decorate([
    typeorm_1.Column("varchar", {
        nullable: false,
        length: 7,
        default: () => "'unidad'",
        name: "unidad_medida"
    }),
    __metadata("design:type", String)
], producto.prototype, "unidad_medida", void 0);
__decorate([
    typeorm_1.Column("tinyint", {
        nullable: false,
        default: () => "'5'",
        name: "stock_critico"
    }),
    __metadata("design:type", Number)
], producto.prototype, "stock_critico", void 0);
__decorate([
    typeorm_1.OneToMany(type => ajuste_inventario_1.ajuste_inventario, ajuste_inventario => ajuste_inventario.codProducto, { onDelete: 'RESTRICT', onUpdate: 'CASCADE' }),
    __metadata("design:type", Array)
], producto.prototype, "ajusteInventarios", void 0);
__decorate([
    typeorm_1.OneToMany(type => detalle_compra_1.detalle_compra, detalle_compra => detalle_compra.codProducto, { onDelete: 'RESTRICT', onUpdate: 'RESTRICT' }),
    __metadata("design:type", Array)
], producto.prototype, "detalleCompras", void 0);
__decorate([
    typeorm_1.OneToMany(type => detalle_venta_1.detalle_venta, detalle_venta => detalle_venta.codProducto, { onDelete: 'RESTRICT', onUpdate: 'RESTRICT' }),
    __metadata("design:type", Array)
], producto.prototype, "detalleVentas", void 0);
__decorate([
    typeorm_1.OneToMany(type => proveedor_producto_1.proveedor_producto, proveedor_producto => proveedor_producto.producto, { onDelete: 'RESTRICT', onUpdate: 'CASCADE' }),
    __metadata("design:type", Array)
], producto.prototype, "proveedorProductos", void 0);
__decorate([
    typeorm_1.OneToMany(type => stock_1.stock, stock => stock.codProducto, { onDelete: 'RESTRICT', onUpdate: 'RESTRICT' }),
    __metadata("design:type", Array)
], producto.prototype, "stocks", void 0);
producto = __decorate([
    typeorm_1.Entity("producto", { schema: "rotiseria" })
], producto);
exports.default = producto;
