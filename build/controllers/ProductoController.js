"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = require("express");
const producto_1 = __importDefault(require("../model/producto"));
class ProductoController {
    getRouter() {
        let router = express_1.Router();
        router.get("/", this.listar);
        router.get("/:id", this.buscar);
        router.post("/", this.agregar);
        router.delete("/:id", this.eliminar);
        router.put("/:id", this.actualizar);
        return router;
    }
    listar(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            producto_1.default.find().then(prods => res.json(prods), error => res.status(500).send(error));
        });
    }
    buscar(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            const { id } = req.params;
            producto_1.default.find({
                where: { cod_producto: id },
                relations: ["proveedorProductos.proveedor", "proveedorProductos"]
            }).then(prods => res.json(prods), error => res.status(500).send(error));
        });
    }
    agregar(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            let producto = req.body;
            res.json(producto);
        });
    }
    eliminar(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            const { id } = req.params;
            producto_1.default.find({ cod_producto: id }).then((prod) => __awaiter(this, void 0, void 0, function* () {
                if (prod.length != 0) {
                    let retorno = yield prod[0].remove();
                    typeof retorno == typeof producto_1.default ? res.send(retorno) : res.status(500).send(retorno);
                }
                else {
                    return res.status(404).send("Producto no encontrado");
                }
            }), (error) => res.status(404).send(error));
        });
    }
    actualizar(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            const { id } = req.params;
            producto_1.default.findOne({ cod_producto: id }).then((producto) => __awaiter(this, void 0, void 0, function* () {
                const prodModificado = req.body;
                prodModificado.cod_producto = id;
                let retorno = prodModificado.save();
                //const producto : Producto = productos[0];
                typeof retorno == typeof prodModificado ? res.send(retorno) : res.status(500).send(retorno);
            }), error => res.status(404).send(error));
        });
    }
}
exports.productoController = new ProductoController();
