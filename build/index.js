"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = __importDefault(require("express"));
const cors_1 = __importDefault(require("cors"));
const morgan_1 = __importDefault(require("morgan"));
const typeorm_1 = require("typeorm");
const ProductoController_1 = require("./controllers/ProductoController");
class Server {
    constructor() {
        this.app = express_1.default();
        this.app.use(morgan_1.default('dev'));
        this.app.use(cors_1.default());
        this.app.use(express_1.default.json());
        this.app.use(express_1.default.urlencoded({ extended: false }));
        this.rutas();
        this.start();
    }
    rutas() {
        this.app.use("/producto", ProductoController_1.productoController.getRouter());
        // this.app.use( "/usuario", usuarioController.getRouter());
    }
    start() {
        typeorm_1.createConnection().then(conexion => {
            this.app.listen("3000", () => {
                console.log("Escuchando en el puerto 3000");
            });
        });
    }
}
new Server();
